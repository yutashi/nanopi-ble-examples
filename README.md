# NanoPi BLE examples
Getting started BLE with NanoPi.

## Environment
- [NanoPi Neo](http://nanopi.io/nanopi-neo.html)
- [Armbian Ubuntu Xenial](https://www.armbian.com/nanopi-neo/)
- [Node.js 6.9.x](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)
- [Bluetooth Dongle](http://buffalo.jp/product/peripheral/wireless-adapter/bsbt4d09bk/)

## Setup

### Bluetooth libraries

```bash
sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev
```

### Bluetooth configuration

You may need to stop `bluetoothd` for prevent interference.

```bash
sudo systemctl stop bluetooth       # once.
sudo systemctl disable bluetooth    # persist on reboot.
```

To power bluetooth adapter up.

```bash
sudo hciconfig hci0 up
```


### Node.js

Armbian doesn't have Node.js by default.

```bash
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
```

## Getting started

Clone the repository.

```bash
git clone https://bitbucket.org/yutashi/nanopi-ble-examples.git
cd nanopi-ble-examples
```

Install dependencies.

```bash
npm install
```

Start advertising.

```bash
node main.js
```

Now you can see `nanopineo` peripheral on your tester (e.g. [Lightblue](https://itunes.apple.com/jp/app/lightblue/id639944780?mt=12))
