const Bleno = require('bleno');

const name = 'nanopi';
const serviceUuids = ['7B3A49B5-B7E0-4892-8FAB-BD4223B2B172']

Bleno.on('stateChange', function(state) {
  console.log('stateChange: ' + state);

  if (state === 'poweredOn') {
    Bleno.startAdvertising(name, serviceUuids, function(err) {
      if (err) {
        throw err;
      }

      console.log('Start Advertising');
    });
  } else {
    Bleno.stopAdvertising(function(err) {
      if (err) {
        throw err;
      }

      console.log('Stop Advertising');
    });
  };
});
